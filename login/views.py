from django.shortcuts import render, redirect
from django.urls import reverse
from django.contrib import messages
from django.db import connection
from balai_pengobatan.views import count_next_id


def home_login(request):
    return render(request, 'home_login.html')


def form_login(request):
    return render(request, 'form_login.html', {
        'failed': request.GET.get('failed')
    })


def form_login_submit(request):
    try:
        email = request.POST['email']
        password = request.POST['password']
        do_login(request, email, password)
        return redirect("profil-home")
    except:
        return redirect(reverse("login:form_login") + "?failed=true")


def home_logout(request):
    request.session.flush()
    return redirect("login:home_login")


def register(request):
    return render(request, 'register.html')


def form_register(request, role):
    list_idapotek = []
    if role == "admin":
        role_pendaftaran = "ADMIN"
        with connection.cursor() as cursor:
            cursor.execute(
                'SELECT id_apotek FROM apotek')
            list_idapotek = [col[0] for col in cursor.fetchall()]
    elif role == "konsumen":
        role_pendaftaran = "KONSUMEN"
    elif role == "kurir":
        role_pendaftaran = "KURIR"
    else:
        role_pendaftaran = "CS"
    return render(request, 'form_register.html', {
        'role_pendaftaran': role_pendaftaran,
        'list_idapotek' : list_idapotek,
        'failed': request.GET.get('failed'),
        'success': request.GET.get('success')
    })


def form_register_konsumen(request):
    try:
        email = request.POST['email']
        password = request.POST['password']
        nama_lengkap = request.POST['nama_lengkap']
        no_telepon = request.POST['no_telepon']
        jenis_kelamin = request.POST['jenis_kelamin']
        tanggal_lahir = request.POST['tanggal_lahir']
        next_konsumen_id = "k" + str(count_next_id("konsumen", "id_konsumen", "k"))

        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM pengguna WHERE email = %s", (email,))
            if len(cursor.fetchall()) > 0:
                return redirect(reverse("login:form_register", args=["konsumen"]) + "?failed=duplicate")

            cursor.execute("INSERT INTO pengguna VALUES (%s, %s, %s, %s)", (
                email, no_telepon, password, nama_lengkap
            ))

            cursor.execute("INSERT INTO konsumen VALUES (%s, %s, %s, %s)", (
                next_konsumen_id, email, jenis_kelamin, tanggal_lahir
            ))

            for key, status_alamat in request.POST.items():
                if key[:14] == 'status_alamat_':
                    nama_alamat = request.POST['nama_alamat_' + key[14:]]
                    cursor.execute("INSERT INTO alamat_konsumen VALUES (%s, %s, %s)", (
                        next_konsumen_id, nama_alamat, status_alamat
                    ))

            do_login(request, email, password)
            return redirect(reverse("login:form_register", args=["konsumen"]) + "?success=true")
    except:
        return redirect(reverse("login:form_register", args=["konsumen"]) + "?failed=true")


def form_register_admin(request):
    try:
        email = request.POST['email']
        password = request.POST['password']
        nama_lengkap = request.POST['nama_lengkap']
        no_telepon = request.POST['no_telepon']
        id_apotek = request.POST['id_apotek']

        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM pengguna WHERE email = %s", (email,))
            if len(cursor.fetchall()) > 0:
                return redirect(reverse("login:form_register", args=["admin"]) + "?failed=duplicate")

            cursor.execute("INSERT INTO pengguna VALUES (%s, %s, %s, %s)", (
                email, no_telepon, password, nama_lengkap
            ))

            cursor.execute("INSERT INTO apoteker VALUES (%s)", (
                email,
            ))

            cursor.execute("INSERT INTO admin_apotek VALUES (%s, %s)", (
                email, id_apotek
            ))

            do_login(request, email, password)
            return redirect(reverse("login:form_register", args=["admin"]) + "?success=true")
    except:
        return redirect(reverse("login:form_register", args=["admin"]) + "?failed=true")


def form_register_kurir(request):
    try:
        email = request.POST['email']
        password = request.POST['password']
        nama_lengkap = request.POST['nama_lengkap']
        no_telepon = request.POST['no_telepon']
        nama_perusahaan = request.POST['id_nama_perusahaan']
        next_kurir_id = "kurir" + str(count_next_id("kurir", "id_kurir", "kurir"))

        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM pengguna WHERE email = %s", (email,))
            if len(cursor.fetchall()) > 0:
                return redirect(reverse("login:form_register", args=["kurir"]) + "?failed=duplicate")

            cursor.execute("INSERT INTO pengguna VALUES (%s, %s, %s, %s)", (
                email, no_telepon, password, nama_lengkap
            ))

            cursor.execute("INSERT INTO kurir VALUES (%s, %s, %s)", (
                next_kurir_id, email, nama_perusahaan
            ))

            do_login(request, email, password)
            return redirect(reverse("login:form_register", args=["kurir"]) + "?success=true")
    except:
        return redirect(reverse("login:form_register", args=["kurir"]) + "?failed=true")


def form_register_cs(request):
    try:
        email = request.POST['email']
        password = request.POST['password']
        nama_lengkap = request.POST['nama_lengkap']
        no_telepon = request.POST['no_telepon']
        no_ktp = request.POST['id_no_ktp']
        no_sia = request.POST['id_no_sia']

        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM pengguna WHERE email = %s", (email,))
            if len(cursor.fetchall()) > 0:
                return redirect(reverse("login:form_register", args=["cs"]) + "?failed=duplicate")

            cursor.execute("INSERT INTO pengguna VALUES (%s, %s, %s, %s)", (
                email, no_telepon, password, nama_lengkap
            ))

            cursor.execute("INSERT INTO apoteker VALUES (%s)", (
                email,
            ))

            cursor.execute("INSERT INTO cs VALUES (%s, %s, %s)", (
                no_ktp, email, no_sia
            ))

            do_login(request, email, password)
            return redirect(reverse("login:form_register", args=["cs"]) + "?success=true")
    except:
        return redirect(reverse("login:form_register", args=["cs"]) + "?failed=true")


# Utility function
def do_login(request, email, password):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM pengguna WHERE email = %s", (email,))
        result = cursor.fetchall()
        if len(result) == 1:
            user = result[0]
            if user[2] == password:
                cursor.execute("SELECT * FROM konsumen WHERE email = %s", (email,))
                role = []
                if len(cursor.fetchall()) == 1:
                    role.append("konsumen")

                cursor.execute("SELECT * FROM apoteker WHERE email = %s", (email,))
                if len(cursor.fetchall()) == 1:
                    role.append("apoteker")

                cursor.execute("SELECT * FROM cs WHERE email = %s", (email,))
                if len(cursor.fetchall()) == 1:
                    role.append("cs")

                cursor.execute("SELECT * FROM admin_apotek WHERE email = %s", (email,))
                if len(cursor.fetchall()) == 1:
                    role.append("admin_apotek")

                cursor.execute("SELECT * FROM kurir WHERE email = %s", (email,))
                if len(cursor.fetchall()) == 1:
                    role.append("kurir")

                if len(role) > 0:
                    request.session['email'] = email
                    request.session['role'] = role
                    return
    
    raise Exception("failed login")
