from django.shortcuts import redirect

def only_pengguna(*role_lst):
    def wrapper(view_func):
        def wrapped(request, *args, **kwargs):
            role = request.session.get('role')
            email = request.session.get('email')
            if role:
                if len(role_lst) == 0:
                    return view_func(request, *args, **kwargs)
                for r in role:
                    if r in role_lst:
                        return view_func(request, *args, **kwargs)
            if email != None:
                return redirect("no_access")
            return redirect("login:home_login")
        return wrapped
    return wrapper
