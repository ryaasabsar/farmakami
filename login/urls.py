"""jonathanjocan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from . import views

app_name = "login"

urlpatterns = [
    path('', views.home_login, name="home_login"),
    path('form-login/', views.form_login, name='form_login'),
    path('form-login-submit/', views.form_login_submit, name='form_login_submit'),
    path('logout/', views.home_logout, name='home_logout'),

    path('register/', views.register, name='register'),
    path('form-register/<str:role>', views.form_register, name='form_register'),
    path('form-register-konsumen/', views.form_register_konsumen, name='form_register_konsumen'),
    path('form-register-admin/', views.form_register_admin, name='form_register_admin'),
    path('form-register-kurir/', views.form_register_kurir, name='form_register_kurir'),
    path('form-register-cs/', views.form_register_cs, name='form_register_cs'),
]
