"""jonathanjocan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from . import views

app_name = "produk_apotek"

urlpatterns = [
    path('', views.rud_produk_apotek, name="rud_produk_apotek"),
    path('update/<str:id_apotek>/<str:id_produk>', views.update_produk_apotek, name='update_produk_apotek'),
    path('delete/<str:id_apotek>/<str:id_produk>', views.delete_produk_apotek, name='delete_produk_apotek'),
    path('create/', views.create_produk_apotek, name='create_produk_apotek'),
]
