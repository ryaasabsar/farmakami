from django.shortcuts import render, redirect
from django.http import HttpResponse  
from django.db import DataError, IntegrityError, connection   
from .forms import *  
from django.contrib import messages
from login.decorators import only_pengguna

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]      

@only_pengguna()
def rud_produk_apotek(request):
    with connection.cursor() as cursor:
        cursor.execute("select * from produk_apotek order by length(id_produk), id_produk")
        rows = dictfetchall(cursor)
    return render(request, 'produk_apotek.html', {'rows':rows})

@only_pengguna("admin_apotek")
def update_produk_apotek(request, id_apotek, id_produk):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            forms = produk_apotek_form(generate_id_apotek_choices(), generate_id_produk_choices(), request.POST)
            if forms.is_valid():
                try:
                    cursor.execute(
                        """update produk_apotek set 
                        harga_jual = %s, 
                        stok = %s,
                        satuan_penjualan = %s,
                        id_produk = %s,
                        id_apotek = %s
                        where id_apotek = %s and id_produk = %s""", 
                        [
                        forms.cleaned_data['harga_jual'],
                        forms.cleaned_data['stok'],
                        forms.cleaned_data['satuan_penjualan'],
                        forms.cleaned_data['id_produk'],
                        forms.cleaned_data['id_apotek'],
                        id_apotek,
                        id_produk
                        ])
                    return redirect('produk_apotek:rud_produk_apotek') 
                except IntegrityError:
                    messages.info(request, "Error: Pasangan id apotek dan id produk tidak boleh sama dengan produk apotek yang lain")
                    return redirect('produk_apotek:update_produk_apotek', id_apotek=id_apotek, id_produk=id_produk)
                except DataError:
                    messages.info(request, "Error: Harga jual atau stok harus berupa angka")
                    return redirect('produk_apotek:update_produk_apotek', id_apotek=id_apotek, id_produk=id_produk)
        else:
            cursor.execute("select * from produk_apotek where id_apotek=%s and id_produk=%s", [id_apotek, id_produk])
            produk_apotek = dictfetchall(cursor)[0]
            forms = produk_apotek_form(generate_id_apotek_choices(), generate_id_produk_choices(),  
                initial = {
                    'id_apotek': produk_apotek['id_apotek'],
                    'id_produk': produk_apotek['id_produk'],
                    'harga_jual': produk_apotek['harga_jual'],
                    'satuan_penjualan': produk_apotek['satuan_penjualan'],
                    'stok': produk_apotek['stok']
                }
            )
            return render(request, 'update_produk_apotek.html', {'forms':forms})

@only_pengguna("admin_apotek")
def delete_produk_apotek(request, id_apotek, id_produk):
    with connection.cursor() as cursor:
        cursor.execute("delete from produk_apotek where id_apotek = %s and id_produk = %s", [id_apotek, id_produk])
        return redirect('produk_apotek:rud_produk_apotek')
    
@only_pengguna("admin_apotek")
def create_produk_apotek(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            forms = produk_apotek_form(generate_id_apotek_choices(), generate_id_produk_choices(), request.POST)
            if forms.is_valid():
                try:
                    cursor.execute(
                        "insert into produk_apotek values(%s, %s, %s, %s, %s)", 
                    [
                    forms.cleaned_data['harga_jual'],
                    forms.cleaned_data['stok'],
                    forms.cleaned_data['satuan_penjualan'],
                    forms.cleaned_data['id_produk'],
                    forms.cleaned_data['id_apotek']               
                    ])
                    return redirect('produk_apotek:rud_produk_apotek') 
                except IntegrityError:
                    messages.info(request, "Error: Pasangan id apotek dan id produk tidak boleh sama dengan produk apotek yang lain")
                    return redirect('produk_apotek:create_produk_apotek')
                except DataError:
                    messages.info(request, "Error: Harga jual atau stok harus berupa angka")
                    return redirect('produk_apotek:create_produk_apotek')
        else:
            forms = produk_apotek_form(generate_id_apotek_choices(), generate_id_produk_choices())
            return render(request, 'create_produk_apotek.html', {'forms':forms})

def generate_id_apotek_choices():
    with connection.cursor() as cursor:
        cursor.execute("select id_apotek from apotek order by length(id_apotek), id_apotek")
        dict_id_apotek = dictfetchall(cursor)

        id_apotek_choices = []

        for id_apotek in dict_id_apotek:
            id_apotek_choices.append((id_apotek['id_apotek'], id_apotek['id_apotek']))
        
        return id_apotek_choices

def generate_id_produk_choices():
    with connection.cursor() as cursor:
        cursor.execute("select id_produk from produk order by length(id_produk), id_produk")
        dict_id_produk = dictfetchall(cursor)

        id_produk_choices = []

        for id_produk in dict_id_produk:
            id_produk_choices.append((id_produk['id_produk'], id_produk['id_produk']))
        
        return id_produk_choices