from django import forms

class produk_apotek_form(forms.Form):

    def __init__(self,id_apotek_choices=None, id_produk_choices=None, *args, **kwargs, ):
        super(produk_apotek_form, self).__init__(*args, **kwargs)
        self.fields['id_apotek'].choices = id_apotek_choices
        self.fields['id_produk'].choices = id_produk_choices

    id_apotek = forms.ChoiceField(
        label = 'Id Apotek',
        choices=()
    )

    id_produk = forms.ChoiceField(
        label = 'Id Produk',
        choices=()
    )
    
    harga_jual = forms.CharField(
        label = 'Harga Jual'
    )

    satuan_penjualan = forms.CharField(
        label = 'Satuan Penjualan',
        max_length = 5
    )

    stok = forms.CharField(
        label = 'Stok'
    )
