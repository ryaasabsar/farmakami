from django.shortcuts import render, redirect
from django.http import HttpResponse   
from django.db import DataError, IntegrityError, connection     
from .forms import *
from django.contrib import messages
from login.decorators import only_pengguna

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

@only_pengguna()
def rud_apotek(request):
    with connection.cursor() as cursor:
        cursor.execute("select * from apotek")
        rows = dictfetchall(cursor)
        return render(request, 'apotek.html', {'rows': rows})

@only_pengguna('admin_apotek')
def update_apotek(request, id_apotek):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            forms = update_apotek_form(request.POST)
            if forms.is_valid():
                try:
                    str_telp = forms.cleaned_data['telp_apotek']
                    str_sia = forms.cleaned_data['no_sia_penyelenggara']
                    int_telp = int(str_telp)    
                    int_sia = int(str_sia)

                    cursor.execute(
                        """update apotek set 
                        email = %s, 
                        no_sia = %s,
                        nama_penyelenggara = %s,
                        nama_apotek = %s,
                        alamat_apotek = %s,
                        telepon_apotek = %s
                        where id_apotek = %s""", 
                        [
                        forms.cleaned_data['email_penyelenggara'],
                        str_sia,
                        forms.cleaned_data['nama_penyelenggara'],
                        forms.cleaned_data['nama_apotek'],
                        forms.cleaned_data['alamat_apotek'],
                        str_telp,
                        id_apotek                 
                        ])
                    return redirect('apotek:rud_apotek') 
                except IntegrityError:
                    messages.info(request, "Error: alamat apotek, email penyelenggara atau no SIA penyelenggara tidak boleh sama dengan apotek yang lain")
                    return redirect('apotek:update_apotek', id_apotek=id_apotek)
                except ValueError:
                    messages.info(request, "Error: no telp apotek atau no SIA penyelenggara harus berupa angka")
                    return redirect('apotek:update_apotek', id_apotek=id_apotek)
        else:
            cursor.execute("select * from apotek where id_apotek=%s", [id_apotek])
            apotek = dictfetchall(cursor)[0]
            forms = update_apotek_form(
                initial = {
                    'id_apotek': id_apotek,
                    'nama_apotek': apotek['nama_apotek'],
                    'alamat_apotek': apotek['alamat_apotek'],
                    'telp_apotek': apotek['telepon_apotek'],
                    'nama_penyelenggara': apotek['nama_penyelenggara'],
                    'no_sia_penyelenggara': apotek['no_sia'],
                    'email_penyelenggara': apotek['email']
                }
            )
            return render(request, 'update_apotek.html', {'forms':forms})

@only_pengguna('admin_apotek')
def delete_apotek(request, id_apotek):
    with connection.cursor() as cursor:
        cursor.execute("delete from apotek where id_apotek = %s", [id_apotek])
        return redirect('apotek:rud_apotek')

@only_pengguna('admin_apotek')
def create_apotek(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            forms = create_apotek_form(request.POST)
            if forms.is_valid():
                try:
                    cursor.execute("select id_apotek as id from apotek")
                    dict_id = dictfetchall(cursor)
                    new_number = int(dict_id[len(dict_id) - 1].get('id')[6:]) + 1
                    new_id = "apotek" + str(new_number)

                    str_telp = forms.cleaned_data['telp_apotek']
                    str_sia = forms.cleaned_data['no_sia_penyelenggara']
                    int_telp = int(str_telp)    
                    int_sia = int(str_sia)
                        
                    cursor.execute("insert into apotek values(%s, %s, %s, %s, %s, %s, %s)", 
                        [new_id,
                        forms.cleaned_data['email_penyelenggara'],
                        str_sia,
                        forms.cleaned_data['nama_penyelenggara'],
                        forms.cleaned_data['nama_apotek'],
                        forms.cleaned_data['alamat_apotek'],
                        str_telp                  
                        ])
                    return redirect('apotek:rud_apotek')
                except IntegrityError:
                    messages.info(request, "Error: alamat apotek, email penyelenggara atau no SIA penyelenggara tidak boleh sama dengan apotek yang lain")
                    return redirect('apotek:create_apotek')
                except ValueError:
                    messages.info(request, "Error: no telp apotek atau no SIA penyelenggara harus berupa angka")
                    return redirect('apotek:create_apotek')
        else:
            forms = create_apotek_form()
            return render(request, 'create_apotek.html', {"forms":forms})