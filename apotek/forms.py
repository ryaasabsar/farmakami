from django import forms

class create_apotek_form(forms.Form):
    nama_apotek = forms.CharField(
        label = 'Nama Apotek',
        max_length = 50
    )

    alamat_apotek = forms.CharField(
        label = 'Alamat Apotek'
    )

    telp_apotek = forms.CharField(
        label = 'No Telp Apotek',
        max_length = 20
    )

    nama_penyelenggara = forms.CharField(
        label = 'Nama Penyelenggara',
        max_length = 50
    )

    no_sia_penyelenggara = forms.CharField(
        label = 'No SIA Penyelenggara',
        max_length = 20
    )

    email_penyelenggara = forms.EmailField(
        label = 'Email Penyelenggara',
        max_length = 50
    )

class update_apotek_form(forms.Form):
    id_apotek = forms.CharField(
        label = 'Id Apotek',
        widget = forms.TextInput(attrs={'readonly': True}),
        
    )

    nama_apotek = forms.CharField(
        label = 'Nama Apotek',
        max_length = 50
    )

    alamat_apotek = forms.CharField(
        label = 'Alamat Apotek'
    )

    telp_apotek = forms.CharField(
        label = 'No Telp Apotek',
        max_length = 20
    )

    nama_penyelenggara = forms.CharField(
        label = 'Nama Penyelenggara',
        max_length = 50
    )

    no_sia_penyelenggara = forms.CharField(
        label = 'No SIA Penyelenggara',
        max_length = 20
    )

    email_penyelenggara = forms.EmailField(
        label = 'Email Penyelenggara',
        max_length = 50
    )
    