from django.urls import path
from . import views

urlpatterns = [
    path('', views.obat_read, name="obat_read"),
    path('create/', views.obat_create, name="obat_create"),
    path('create-submit/', views.obat_create_submit, name="obat_create_submit"),
    path('update/<str:id_obat>', views.obat_update, name="obat_update"),
    path('update-submit/', views.obat_update_submit, name="obat_update_submit"),
    path('delete/<str:id_obat>', views.obat_delete, name="obat_delete"),
]
