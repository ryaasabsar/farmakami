from django.shortcuts import render, redirect
from django.db import connection
from django.urls import reverse
from login.decorators import only_pengguna
from balai_pengobatan.views import count_next_id


@only_pengguna()
def obat_read(request):
    header_lst = []
    data_lst = []
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM obat')
        header_lst = [col[0] for col in cursor.description]
        data_lst = cursor.fetchall()

    return render(request, 'obat_read.html', {
        'header': header_lst,
        'data': data_lst,
        'faildelete': request.GET.get('faildelete'),
        'success': request.GET.get('success')
    })


@only_pengguna('apoteker')
def obat_update(request, id_obat):
    header_lst = []
    data_lst = []
    merk_obat_lst = []
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM obat WHERE id_obat = '{}'".format(id_obat))
        header_lst = [col[0] for col in cursor.description]
        data_lst = cursor.fetchall()[0]

        cursor.execute('SELECT id_merk_obat FROM merk_obat')
        merk_obat_lst = [col[0] for col in cursor.fetchall()]

    return render(request, 'obat_update.html', {
        'header': header_lst,
        'data': data_lst,
        'merk_obat_lst': merk_obat_lst,
        'failed': request.GET.get('failed')
    })


@only_pengguna('apoteker')
def obat_update_submit(request):
    try:
        with connection.cursor() as cursor:
            cursor.execute(
'''
UPDATE obat SET id_merk_obat = %s, netto = %s, dosis = %s, aturan_pakai = %s,
    kontradiksi = %s, bentuk_kesediaan = %s
WHERE id_obat = %s
''', (
                request.POST['id_merk_obat'], request.POST['netto'], request.POST['dosis'],
                request.POST['aturan_pakai'], request.POST['kontraindikasi'],
                request.POST['bentuk_kesediaan'], request.POST['id_obat'],
            ))

        return redirect(reverse("obat_read") + "?success=update")
    except:
        id_obat = request.POST.get('id_obat')
        if id_obat:
            return redirect(reverse("obat_update", args=[id_obat]) + "?failed=true")
        else:
            return redirect('obat_read')


@only_pengguna('apoteker')
def obat_delete(request, id_obat):
    try:
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM obat WHERE id_obat = %s", (id_obat,))
        return redirect(reverse('obat_read') + "?success=delete")
    except:
        return redirect(reverse('obat_read') + "?faildelete={}".format(id_obat))


@only_pengguna('apoteker')
def obat_create(request):
    merk_obat_lst = []
    with connection.cursor() as cursor:
        cursor.execute('SELECT id_merk_obat FROM merk_obat')
        merk_obat_lst = [col[0] for col in cursor.fetchall()]

    return render(request, 'obat_create.html', {
        'merk_obat_lst': merk_obat_lst,
        'failed': request.GET.get('failed')
    })


@only_pengguna('apoteker')
def obat_create_submit(request):
    try:
        next_obat = "med" + str(count_next_id("obat", "id_obat", "med"))
        next_produk = "obat" + str(count_next_id("produk", "id_produk", "obat"))

        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO produk VALUES (%s)', (next_produk,))

            cursor.execute('INSERT INTO obat VALUES (%s, %s, %s, %s, %s, %s, %s, %s)', (
                next_obat, next_produk, request.POST['id_merk_obat'], request.POST['netto'],
                request.POST['dosis'], request.POST['aturan_pakai'], request.POST['kontraindikasi'],
                request.POST['bentuk_kesediaan']
            ))
        return redirect(reverse("obat_read") + "?success=create")
    except:
        return redirect(reverse("obat_create") + "?failed=true")
