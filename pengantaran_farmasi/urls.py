from django.urls import path
from . import views

urlpatterns = [
    path('', views.pengantaran_farmasi_read, name='pf-rud'),
    path('create/', views.pengantaran_farmasi_create, name='pf-create'),
    path('create/submit/', views.pengantaran_farmasi_create_submit, name="pf-create-submit"),
    path('update/<str:idpengantaran>', views.pengantaran_farmasi_update, name='pf-update'),
    path('update/submit/', views.pengantaran_farmasi_update_submit, name='pf-update-submit'),
    path('delete/<str:idpengantaran>', views.pengantaran_farmasi_delete, name="pf-delete"),
]
