from django.shortcuts import render, redirect
from django.db import connection, ProgrammingError, DataError
from balai_pengobatan.views import count_next_id
from datetime import datetime, timedelta
from django.contrib import messages
from login.decorators import only_pengguna


@only_pengguna()
def pengantaran_farmasi_read(request):
    pengantaran_farmasi = connection.cursor()
    pengantaran_farmasi.execute('SELECT * FROM pengantaran_farmasi ORDER BY id_pengantaran')
    data = pengantaran_farmasi.fetchall()

    return render(request, 'pengantaran_farmasi_read.html', {
        'data': data
    })


@only_pengguna('admin_apotek')
def pengantaran_farmasi_update(request, idpengantaran):
    # Asumsi mengikuti ID Pengantaran, ID Kurir dan ID Transaksi tidak diharapkan bisa diganti
    pengantaran_farmasi = connection.cursor()
    command = "SELECT * FROM pengantaran_farmasi " \
              "WHERE id_pengantaran = '{}'".format(idpengantaran)
    pengantaran_farmasi.execute(command)
    data = pengantaran_farmasi.fetchone()
    idtransaksi = data[2]

    timezoneJakarta = datetime.now() + timedelta(hours=7)
    waktu = timezoneJakarta.strftime('%Y-%m-%d %H:%M:%S')

    pengantaran_farmasi.execute(
        "SELECT total_pembayaran FROM transaksi_pembelian WHERE id_transaksi_pembelian = '{}'".format(idtransaksi))
    biaya_transaksi = pengantaran_farmasi.fetchone()[0]

    return render(request, 'pengantaran_farmasi_update.html', {
        'data': data,
        'waktu': waktu,
        'biaya_transaksi': biaya_transaksi
    })


@only_pengguna('admin_apotek')
def pengantaran_farmasi_update_submit(request):
    try:
        pengantaran_farmasi = connection.cursor()
        if (len(request.POST['status']) <= 20):
            command = "UPDATE pengantaran_farmasi " \
                      "SET waktu = '{waktu}', status_pengantaran = '{status}', biaya_kirim = {biayakirim} " \
                      "WHERE id_pengantaran = '{pengantaran}'".format(
                waktu=request.POST['waktu'], status=request.POST['status'],
                biayakirim=request.POST['biayakirim'], pengantaran=request.POST['pengantaran'])
            pengantaran_farmasi.execute(command)
            return redirect('pf-rud')
        else:
            messages.info(request, "Jumlah karakter status pengantaran lebih dari 20 karakter ")
            return redirect('pf-update', idpengantaran=request.POST['pengantaran'])
    except ProgrammingError:
        messages.info(request, "Kemungkinan error: ")
        messages.info(request, "- Field Biaya Kirim belum diisi")
        messages.info(request, "- Kesalahan input pada field Biaya Kirim (hanya menerima integer)")
        return redirect('pf-update', idpengantaran=request.POST['pengantaran'])


@only_pengguna('admin_apotek')
def pengantaran_farmasi_delete(request, idpengantaran):
    pengantaran_farmasi = connection.cursor()
    command = "DELETE FROM pengantaran_farmasi WHERE id_pengantaran = '{}'".format(idpengantaran)
    pengantaran_farmasi.execute(command)
    return redirect('pf-rud')


@only_pengguna('admin_apotek')
def pengantaran_farmasi_create(request):
    pengantaran_farmasi = connection.cursor()
    pengantaran_farmasi.execute(
        'SELECT id_transaksi_pembelian FROM transaksi_pembelian ORDER BY id_transaksi_pembelian')
    list_idtransaksi = [col[0] for col in pengantaran_farmasi.fetchall()]

    return render(request, 'pengantaran_farmasi_create.html', {
        'list_idtransaksi': list_idtransaksi
    })


@only_pengguna('admin_apotek')
def pengantaran_farmasi_create_submit(request):
    try:
        idpengantaran = "antar" + str(count_next_id("pengantaran_farmasi", "id_pengantaran", "antar"))
        idkurir = "kurir01"  # ID kurir default
        status = "SHIPPING"  # Status pengantaran default

        pengantaran_farmasi = connection.cursor()
        command = "INSERT INTO pengantaran_farmasi VALUES ('{idpengantaran}','{idkurir}','{transaksi}','{waktu}','{status}',{biayakirim})".format(
            idpengantaran=idpengantaran, idkurir=idkurir, transaksi=request.POST['transaksi'],
            waktu=request.POST['waktu'], status=status, biayakirim=request.POST['biayakirim'])
        pengantaran_farmasi.execute(command)
        return redirect('pf-rud')
    except ProgrammingError:
        messages.info(request, "Kemungkinan error: ")
        messages.info(request, "- Field Biaya Kirim belum diisi")
        messages.info(request, "- Kesalahan input pada field Biaya Kirim (hanya menerima integer)")
        return redirect('pf-create')
    except DataError:
        messages.info(request, "Kemungkinan error: ")
        messages.info(request, "- Field Waktu belum diisi")
        messages.info(request, "- Kesalahan input pada field Waktu (format: YYYY-MM-DD HH:MM:SS)")
        return redirect('pf-create')
