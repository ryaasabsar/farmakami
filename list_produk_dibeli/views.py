from django.shortcuts import render, redirect
from django.db import connection, IntegrityError, ProgrammingError
from django.contrib import messages
from login.decorators import only_pengguna


@only_pengguna()
def produk_dibeli_read(request):
    produk_dibeli = connection.cursor()
    produk_dibeli.execute('SELECT * FROM list_produk_dibeli ORDER BY id_transaksi_pembelian')
    data = produk_dibeli.fetchall()
    return render(request, 'produk_dibeli_read.html', {'data': data})


@only_pengguna('admin_apotek')
def produk_dibeli_update(request, apotek, produk, transaksi):
    produk_dibeli = connection.cursor()
    command = "SELECT * FROM list_produk_dibeli " \
              "WHERE id_apotek = '{apotek}' AND id_produk = '{produk}' AND id_transaksi_pembelian='{transaksi}'".format(
        apotek=apotek, produk=produk, transaksi=transaksi)
    produk_dibeli.execute(command)
    data = produk_dibeli.fetchone()

    produk_dibeli.execute('SELECT id_transaksi_pembelian FROM transaksi_pembelian ORDER BY id_transaksi_pembelian')
    list_idtransaksi = [col[0] for col in produk_dibeli.fetchall()]

    return render(request, 'produk_dibeli_update.html', {
        'data': data,
        'list_idtransaksi': list_idtransaksi
    })


@only_pengguna('admin_apotek')
def produk_dibeli_update_submit(request, transaksilama):
    try:
        produk_dibeli = connection.cursor()
        command = "UPDATE list_produk_dibeli " \
                  "SET jumlah = {jumlah}, id_transaksi_pembelian = '{baru}'" \
                  "WHERE id_apotek = '{apotek}' AND id_produk = '{produk}' AND id_transaksi_pembelian = '{lama}'".format(
            jumlah=request.POST['jumlah'], baru=request.POST['transaksi'],
            apotek=request.POST['apotek'], produk=request.POST['produk'], lama=transaksilama)
        produk_dibeli.execute(command)
        return redirect('pd-rud')
    except ProgrammingError:
        messages.info(request, "Kemungkinan error: ")
        messages.info(request, "- Field Jumlah belum diisi")
        messages.info(request, "- Kesalahan input pada field Jumlah (hanya menerima integer)")
        return redirect('pd-update', apotek=request.POST['apotek'], produk=request.POST['produk'],
                        transaksi=transaksilama)


@only_pengguna('admin_apotek')
def produk_dibeli_delete(request, apotek, produk, transaksi):
    produk_dibeli = connection.cursor()
    command = "DELETE FROM list_produk_dibeli " \
              "WHERE id_apotek = '{apotek}' AND id_produk = '{produk}' AND id_transaksi_pembelian = '{transaksi}'".format(
        apotek=apotek, produk=produk, transaksi=transaksi)
    produk_dibeli.execute(command)
    return redirect('pd-rud')


@only_pengguna('admin_apotek')
def produk_dibeli_create(request):
    produk_dibeli = connection.cursor()
    produk_dibeli.execute('SELECT DISTINCT id_produk FROM produk_apotek ORDER BY id_produk')
    list_idproduk = [col[0] for col in produk_dibeli.fetchall()]

    produk_dibeli.execute('SELECT DISTINCT id_apotek FROM produk_apotek ORDER BY id_apotek')
    list_idapotek = [col[0] for col in produk_dibeli.fetchall()]

    produk_dibeli.execute('SELECT id_transaksi_pembelian FROM transaksi_pembelian ORDER BY id_transaksi_pembelian')
    list_idtransaksi = [col[0] for col in produk_dibeli.fetchall()]

    return render(request, 'produk_dibeli_create.html', {
        'list_idproduk': list_idproduk,
        'list_idapotek': list_idapotek,
        'list_idtransaksi': list_idtransaksi
    })


@only_pengguna('admin_apotek')
def produk_dibeli_create_submit(request):
    try:
        produk_dibeli = connection.cursor()
        command = "INSERT INTO list_produk_dibeli VALUES ({jumlah},'{apotek}','{produk}','{transaksi}')".format(
            jumlah=request.POST['jumlah'], apotek=request.POST['apotek'],
            produk=request.POST['produk'], transaksi=request.POST['transaksi'])
        produk_dibeli.execute(command)
        return redirect('pd-rud')
    except IntegrityError:
        messages.info(request, "Kemungkinan error: ")
        messages.info(request,
                      "- " + request.POST['apotek'].capitalize() + " tidak menjual produk " + request.POST['produk'])
        messages.info(request,
                      "- Duplikasi key (Sebelumnya telah ada pembelian " + request.POST['apotek'] + " - " +
                      request.POST[
                          'produk'] + " pada transaksi " + request.POST['transaksi'] + ")")
        return redirect('pd-create')
    except ProgrammingError:
        messages.info(request, "Kemungkinan error: ")
        messages.info(request, "- Field Jumlah belum diisi")
        messages.info(request, "- Kesalahan input pada field Jumlah (hanya menerima integer)")
        return redirect('pd-create')
