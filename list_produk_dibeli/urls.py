from django.urls import path
from . import views

urlpatterns = [
    path('', views.produk_dibeli_read, name='pd-rud'),
    path('create/', views.produk_dibeli_create, name='pd-create'),
    path('create/submit/', views.produk_dibeli_create_submit, name="pd-create-submit"),
    path('update/<str:apotek>/<str:produk>/<str:transaksi>/', views.produk_dibeli_update, name='pd-update'),
    path('update/submit/<str:transaksilama>/', views.produk_dibeli_update_submit, name='pd-update-submit'),
    path('delete/<str:apotek>/<str:produk>/<str:transaksi>/', views.produk_dibeli_delete, name="pd-delete"),
]
