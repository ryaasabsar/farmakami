FarmaKami
============

[![pipeline status](https://gitlab.com/basdat48/farmakami/badges/master/pipeline.svg)](https://gitlab.com/basdat48/farmakami/-/commits/master)

## Kelompok 48 Basis Data
- Thami Endamora (1806141460)
- Nanda Ryaas Absar (1806141391)
- Firdaus Al-Ghifari (1806133780)
- Jonathan (1806204985)

## Link HerokuApp :
https://tk4-basdat-kel48.herokuapp.com/
