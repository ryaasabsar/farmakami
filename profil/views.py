from django.shortcuts import render, redirect
from django.http import HttpResponse  
from django.db import connection   
from login.decorators import only_pengguna
from datetime import datetime

# Create your views here.
@only_pengguna()
def home(request):
    response = {}
    with connection.cursor() as cursor:
        email = request.session.get('email')
        role = request.session.get('role')

        cursor.execute("SELECT * FROM pengguna WHERE email = %s", (email,))
        result = cursor.fetchall()[0]
        response["nama_pengguna"] = result[3]
        response["no_telepon"] = result[1]
        if "admin_apotek" in role:
            response["role"] = "Admin"
        elif "kurir" in role:
            cursor.execute("SELECT * FROM kurir WHERE email = %s", (email,))
            result = cursor.fetchall()[0]
            response["nama_perusahaan"] = result[2]
            response["role"] = "Kurir"
        elif "cs" in role:
            cursor.execute("SELECT * FROM cs WHERE email = %s", (email,))
            result = cursor.fetchall()[0]
            response["no_ktp"] = result[0]
            response["no_sia"] = result[2]
            response["role"] = "CS"
        elif "konsumen" in role:
            cursor.execute("SELECT * FROM konsumen WHERE email = %s", (email,))
            result = cursor.fetchall()[0]
            id_konsumen = result[0]
            response["jenis_kelamin"] = "Laki-laki" if result[2] == "M" else "Perempuan"
            response["tgl_lahir"] = date_converter(result[3])
            
            cursor.execute("SELECT * FROM alamat_konsumen WHERE id_konsumen = %s", (id_konsumen,))
            result = cursor.fetchall()
            alamat_list = []
            for alamat in result:
                alamat_list.append(alamat[1])
            response["alamat_list"] = alamat_list
            response["role"] = "Konsumen"

    return render(request, 'profil/home.html', response)

def date_converter(date):
    day = date.day
    month = date.month
    year = date.year

    month_list = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
                    "Juli", "Agustus", "September", "Oktober", "November", "Desember"]

    result = "{0} {1} {2}".format(str(day), month_list[month-1], str(year))
    return result