from django.shortcuts import render, redirect
from django.db import connection
from django.urls import reverse
from login.decorators import only_pengguna


def count_next_id(table_name, col_name, prefix):
    with connection.cursor() as cursor:
        cursor.execute("SELECT {} FROM {}".format(col_name, table_name))
        rows = cursor.fetchall()
        if len(rows) == 0:
            return 1
        prefix_len = len(prefix)
        return max(int(row[0][prefix_len:]) for row in rows if row[0][:prefix_len] == prefix) + 1
    return None


@only_pengguna()
def balai_pengobatan_read(request):
    header_lst = []
    data_lst = []
    with connection.cursor() as cursor:
        cursor.execute(
'''
SELECT bp.id_balai, alamat_balai, nama_balai, jenis_balai,
telepon_balai, string_agg AS apotek_yang_berasosiasi
FROM balai_pengobatan bp LEFT JOIN (
SELECT id_balai, string_agg(nama_apotek, ',')
FROM balai_apotek ba, apotek a
WHERE ba.id_apotek = a.id_apotek
GROUP BY id_balai
) AS x
ON bp.id_balai = x.id_balai
ORDER BY bp.id_balai;
''')
        header_lst = [col[0] for col in cursor.description]
        data_lst = [list(col) for col in cursor.fetchall()]
        for data in data_lst:
            if data[-1]:
                data[-1] = data[-1].split(',')

    return render(request, 'balai_pengobatan_read.html', {
        'header': header_lst,
        'data': data_lst,
        'faildelete': request.GET.get('faildelete'),
        'success': request.GET.get('success')
    })


@only_pengguna('admin_apotek')
def balai_pengobatan_update(request, id_balai):
    header_lst = []
    data_lst = []
    apotek_lst = []
    with connection.cursor() as cursor:
        cursor.execute('SELECT id_apotek FROM apotek')
        apotek_lst = [[col[0], False] for col in cursor.fetchall()]

        cursor.execute(
'''
SELECT bp.id_balai, alamat_balai, nama_balai, jenis_balai,
  telepon_balai, string_agg AS apotek_yang_berasosiasi
FROM balai_pengobatan bp LEFT JOIN (
  SELECT id_balai, string_agg(a.id_apotek, ',')
  FROM balai_apotek ba, apotek a
  WHERE ba.id_apotek = a.id_apotek
  GROUP BY id_balai
) AS x
ON bp.id_balai = x.id_balai
WHERE bp.id_balai = %s;
''', (id_balai,))
        header_lst = [col[0] for col in cursor.description]
        data_lst = list(cursor.fetchall()[0])
        if data_lst[-1]:
            current_apotek_lst = data_lst[-1].split(',')
            for apotek in apotek_lst:
                if apotek[0] in current_apotek_lst:
                    apotek[1] = True

    return render(request, 'balai_pengobatan_update.html', {
        'header': header_lst,
        'data': data_lst,
        'apotek_lst': apotek_lst,
        'failed': request.GET.get('failed')
    })


@only_pengguna('admin_apotek')
def balai_pengobatan_update_submit(request):
    try:
        with connection.cursor() as cursor:
            cursor.execute(
'''
UPDATE balai_pengobatan SET alamat_balai = %s, nama_balai = %s,
    jenis_balai = %s, telepon_balai = %s WHERE id_balai = %s;
''', (
                request.POST['alamat_balai'], request.POST['nama_balai'],
                request.POST['jenis_balai'], request.POST['telepon_balai'],
                request.POST['id_balai'],
            ))

            cursor.execute("DELETE FROM balai_apotek WHERE id_balai = %s", (
                request.POST['id_balai'],
            ))

            for id_apotek in request.POST.getlist('apotek_yang_berasosiasi[]'):
                cursor.execute('INSERT INTO balai_apotek VALUES (%s, %s)', (
                    request.POST['id_balai'], id_apotek,
                ))
        
        return redirect(reverse("balai_pengobatan_read") + "?success=update")
    except:
        id_balai = request.POST.get('id_balai')
        if id_balai:
            return redirect(reverse("balai_pengobatan_update", args=[id_balai]) + "?failed=true")
        else:
            return redirect('balai_pengobatan_read')


@only_pengguna('admin_apotek')
def balai_pengobatan_delete(request, id_balai):
    try:
        with connection.cursor() as cursor:
            cursor.execute("DELETE FROM balai_pengobatan WHERE id_balai = %s", (id_balai,))
        return redirect(reverse("balai_pengobatan_read") + "?success=delete")
    except:
        return redirect(reverse('balai_pengobatan_read') + "?faildelete={}".format(id_balai))


@only_pengguna('admin_apotek')
def balai_pengobatan_create(request):
    apotek_lst = []
    with connection.cursor() as cursor:
        cursor.execute('SELECT id_apotek FROM apotek')
        apotek_lst = [col[0] for col in cursor.fetchall()]

    return render(request, 'balai_pengobatan_create.html', {
        'apotek_lst': apotek_lst,
        'failed': request.GET.get('failed')
    })


@only_pengguna('admin_apotek')
def balai_pengobatan_create_submit(request):
    try:
        rows_count = count_next_id("balai_pengobatan", "id_balai", "balai")
        next_id = "balai0" + str(rows_count)
        if rows_count // 10 > 0:
            next_id = "balai" + str(rows_count)

        with connection.cursor() as cursor:
            cursor.execute('INSERT INTO balai_pengobatan VALUES (%s, %s, %s, %s, %s)', (
                next_id, request.POST['alamat_balai'], request.POST['nama_balai'],
                request.POST['jenis_balai'], request.POST['telepon_balai'],
            ))

            for id_apotek in request.POST.getlist('apotek_yang_berasosiasi[]'):
                cursor.execute('INSERT INTO balai_apotek VALUES (%s, %s)', (
                    next_id, id_apotek,
                ))
        
        return redirect(reverse("balai_pengobatan_read") + "?success=create")
    except:
        return redirect(reverse("balai_pengobatan_create") + "?failed=true")
