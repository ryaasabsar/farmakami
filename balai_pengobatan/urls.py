from django.urls import path
from . import views

urlpatterns = [
    path('', views.balai_pengobatan_read, name="balai_pengobatan_read"),
    path('create/', views.balai_pengobatan_create, name="balai_pengobatan_create"),
    path('create-submit/', views.balai_pengobatan_create_submit, name="balai_pengobatan_create_submit"),
    path('update/<str:id_balai>', views.balai_pengobatan_update, name="balai_pengobatan_update"),
    path('update-submit/', views.balai_pengobatan_update_submit, name="balai_pengobatan_update_submit"),
    path('delete/<str:id_balai>', views.balai_pengobatan_delete, name="balai_pengobatan_delete"),
]
