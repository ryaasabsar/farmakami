from django.shortcuts import render, redirect
from django.http import HttpResponse  
from django.db import connection, IntegrityError, ProgrammingError
from django.contrib import messages 
from login.decorators import only_pengguna
from .forms import *
from datetime import datetime

# Create your views here.
def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]   

@only_pengguna()
def read(request):
    response = {}
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM transaksi_pembelian")
        data = dictfetchall(cursor)
        for dic in data:
            dic["waktu_pembelian"] = convert_datetime(dic["waktu_pembelian"])
        response['data'] = data
    
    return render(request, "transaksi_pembelian/read.html", response)

@only_pengguna("admin_apotek")
def create(request):
    response = {}
    if request.method == "POST":
        try:
            with connection.cursor() as cursor:
                now = datetime.now()
                command = "INSERT INTO transaksi_pembelian VALUES " \
                    "('{id_transaksi}','{waktu}','{harga}','{id_konsumen}')".format(
                        id_transaksi = generate_new_id_transaksi(),
                        waktu = now,
                        harga = 0,
                        id_konsumen = request.POST['id_konsumen']
                    ) 
                cursor.execute(command)
                messages.info(request, "Sukses menambahkan transaksi pembelian")
                return redirect('tbeli-read') 
        except ProgrammingError:
            messages.info(request, "Transaksi pembelian sudah mencapai limit")
            return redirect('tbeli-create')
        except IntegrityError:
            messages.info(request, "Sudah ada id transaksi yang sama")
            return redirect('tbeli-create')
    else:
        response['form'] = CreateForm(generate_list_id_konsumen())
        return render(request, "transaksi_pembelian/create.html", response)

@only_pengguna("admin_apotek")
def update(request, id_transaksi_pembelian):
    response = {}
    if request.method == "POST":
        try:
            with connection.cursor() as cursor:
                now = datetime.now()
                command = ''' UPDATE transaksi_pembelian 
                        SET id_konsumen = '{id_konsumen}', waktu_pembelian = '{now}' 
                        WHERE id_transaksi_pembelian = '{id_transaksi}' '''.format(
                            id_konsumen = request.POST["id_konsumen"],
                            now = now,
                            id_transaksi = id_transaksi_pembelian
                        )
                cursor.execute(command)
                messages.info(request, "Sukses meng-update transaksi pembelian")
                return redirect('tbeli-read') 
        except ProgrammingError:
            messages.info(request, "Transaksi pembelian sudah mencapai limit")
            return redirect('tbeli-update')
        except IntegrityError:
            messages.info(request, "Sudah ada id transaksi yang sama")
            return redirect('tbeli-update')
    else:
        with connection.cursor() as cursor:
            command = '''SELECT * FROM transaksi_pembelian  
                        WHERE id_transaksi_pembelian = '{}' '''.format(id_transaksi_pembelian)
            cursor.execute(command)
            data = cursor.fetchall()[0]
            print(command)
            form = UpdateForm(generate_list_id_konsumen(), initial = {
                'id_transaksi' : id_transaksi_pembelian,
                'waktu_pembelian' : convert_datetime(data[1]),
                'total_pembayaran' : data[2],
            })
            response['form'] = form
        return render(request, "transaksi_pembelian/update.html", response)

@only_pengguna("admin_apotek")
def delete(request, id_transaksi_pembelian):
    with connection.cursor() as cursor:
        command = '''DELETE FROM transaksi_pembelian  
                    WHERE id_transaksi_pembelian = '{}' '''.format(id_transaksi_pembelian)
        cursor.execute(command)
        messages.info(request, "Sukses menghapus transaksi pembelian")
    return redirect("tbeli-read")

def convert_datetime(datetime):
    day = datetime.day
    month = datetime.month
    year = datetime.year
    hour = datetime.hour
    minute = datetime.minute
    second = datetime.second

    month_list = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
                    "Juli", "Agustus", "September", "Oktober", "November", "Desember"]

    result = "{0} {1} {2} {3:0>2d}:{4:0>2d}:{5:0>2d}".format(
        str(day), month_list[month-1], str(year),
        hour, minute, second)
    return result


def generate_list_id_konsumen():
    result = []
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_konsumen FROM konsumen ORDER BY LENGTH(id_konsumen), id_konsumen")
        data = cursor.fetchall()
        for row in data:
            id_konsumen = row[0]
            result.append((id_konsumen, id_konsumen))
    return result

def generate_new_id_transaksi():
    result = "beli"
    with connection.cursor() as cursor:
        cursor.execute("SELECT id_transaksi_pembelian FROM transaksi_pembelian ORDER BY LENGTH(id_transaksi_pembelian), id_transaksi_pembelian DESC LIMIT 1")
        data = cursor.fetchall()[0]
        latest_id_transaksi = data[0]
        result += str(int(latest_id_transaksi[4:]) + 1)
    return result            