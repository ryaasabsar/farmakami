from django.urls import path
from . import views

urlpatterns = [
    path('', views.read, name='tbeli-read'),
    path('create/', views.create, name='tbeli-create'),
    path('update/<str:id_transaksi_pembelian>', views.update, name='tbeli-update'),
    path('delete/<str:id_transaksi_pembelian>', views.delete, name='tbeli-delete'),
]
