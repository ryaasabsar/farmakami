from django import forms

class CreateForm(forms.Form):
    def __init__(self,id_konsumen_choices=[], *args, **kwargs, ):
        super(CreateForm, self).__init__(*args, **kwargs)
        self.fields['id_konsumen'].choices = id_konsumen_choices
    
    id_konsumen = forms.ChoiceField(label='ID Konsumen', choices=(), widget=forms.Select)

class UpdateForm(forms.Form):
    def __init__(self,id_konsumen_choices=[], *args, **kwargs, ):
        super(UpdateForm, self).__init__(*args, **kwargs)
        self.fields['id_konsumen'].choices = id_konsumen_choices

    id_transaksi = forms.CharField(label='ID Transaksi', widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'type' : 'text',
        'readonly' : True
    }))
    waktu_pembelian = forms.CharField(label='Waktu Pembelian', widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'type' : 'text',
        'readonly' : True
    }))
    total_pembayaran = forms.CharField(label='Total Pembayaran', widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'type' : 'text',
        'readonly' : True
    }))

    id_konsumen = forms.ChoiceField(label='ID Konsumen', choices=(), widget=forms.Select)